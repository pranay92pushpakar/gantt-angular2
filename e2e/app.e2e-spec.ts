import { GanttAngular2Page } from './app.po';

describe('gantt-angular2 App', () => {
  let page: GanttAngular2Page;

  beforeEach(() => {
    page = new GanttAngular2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
