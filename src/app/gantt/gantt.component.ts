import { LinkService } from '../services/link-service.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import "dhtmlx-gantt";
import { } from "@types/dhtmlxgantt";
import { TaskService } from '../services/task-service.service';

@Component({
  selector: 'app-gantt',
  templateUrl: './gantt.component.html',
  styleUrls: ['./gantt.component.css']
})
export class GanttComponent implements OnInit {
  ganttData;
  selectedData;

  constructor(private taskService: TaskService, private linkService: LinkService) { }

  @ViewChild("gantt_here") ganttContainer: ElementRef;

  ngOnInit() {
    gantt.config.xml_date = "%Y-%m-%d %H:%i";

    gantt.init(this.ganttContainer.nativeElement);

    gantt.attachEvent("onTaskClick", (id, item) => {
      //this.linkService.update(this.serializeLink(item));
      console.log("id"+ id, "item",this.ganttData);
      const databyId = this.ganttData.filter(ganttData => ganttData.id == id);
      console.log("databyId", databyId);
      this.selectedData = databyId;
    });

    Promise.all([this.taskService.get(), this.linkService.get()])
      .then(([data, links]) => {
        this.ganttData = data;
        gantt.parse({ data, links });
      });
  }

}
