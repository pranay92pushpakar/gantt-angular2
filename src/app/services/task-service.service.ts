import {Injectable} from "@angular/core";
import {Task} from "../models/task";

@Injectable()
export class TaskService {
    get(): Promise<Task[]>{
        return Promise.resolve([
            {id: 1, text: "Task #1", start_date: "2017-04-15 00:00", duration: 3, progress: 0.6},
            {id: 2, text: "Task #2", start_date: "2017-04-18 00:00", duration: 3, progress: 0.4},
            {id: 3, text: "Task #3", start_date: "2017-04-21 00:00", duration: 2, progress: 0.4},
            {id: 4, text: "Task #4", start_date: "2017-04-23 00:00", duration: 2, progress: 0.4},
            {id: 5, text: "Task #5", start_date: "2017-04-25 00:00", duration: 3, progress: 0.4},
            {id: 6, text: "Task #6", start_date: "2017-04-28 00:00", duration: 3, progress: 0.4}
        ]);
    }
}