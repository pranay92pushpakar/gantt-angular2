import {Injectable} from "@angular/core";
import {Link} from "../models/link";

@Injectable()
export class LinkService {
    get(): Promise<Link[]> {
        return Promise.resolve([
            {id: 1, source: 1, target: 2, type: "0"},
            {id: 2, source: 2, target: 3, type: "0"},
            {id: 3, source: 3, target: 4, type: "0"},
            {id: 4, source: 4, target: 5, type: "0"},
            {id: 5, source: 5, target: 6, type: "0"}
        ]);
    }
}