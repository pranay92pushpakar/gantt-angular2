import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttDetailsComponent } from './gantt-details.component';

describe('GanttDetailsComponent', () => {
  let component: GanttDetailsComponent;
  let fixture: ComponentFixture<GanttDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanttDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
