import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gantt-details',
  templateUrl: './gantt-details.component.html',
  styleUrls: ['./gantt-details.component.css']
})
export class GanttDetailsComponent implements OnInit {

  @Input() ganttDataMessage: any;
  constructor() { }

  ngOnInit() {
    console.log("ganttDataMessage", this.ganttDataMessage )
  }

}
