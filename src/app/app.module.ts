import { LinkService } from './services/link-service.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { GanttComponent } from './gantt/gantt.component';
import { TaskService } from './services/task-service.service';
import { GanttDetailsComponent } from './gantt-details/gantt-details.component';

@NgModule({
  declarations: [
    AppComponent,
    GanttComponent,
    GanttDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [LinkService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
